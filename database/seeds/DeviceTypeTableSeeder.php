<?php

use Illuminate\Database\Seeder;

class DeviceTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::insert("INSERT INTO `device_types` (`id`, `name`, `manufacturer`, `model_number`, `device_origin_id`, `created_at`, `updated_at`) VALUES
        (1, 'laptop', 'Lenovo', 'G40-45', '1', '2019-08-29 12:55:52', '2019-08-29 12:55:52'),
        (2, 'laptop', 'Lenovo', 'IdeaPad 100S-14IBR', '1', '2019-08-29 12:57:52', '2019-08-29 12:56:52'),
        (3, 'Laptop', 'Lenovo', 'IdeaPad 310S-11IAP-1GID', '2', '2019-08-29 14:58:59', '2019-08-29 15:59:52'),
        (4, 'Laptop', 'Asus', 'Zenbook 3', '1', '2019-08-29 12:58:59', '2019-08-29 13:59:52'),        
        (5, 'Laptop', 'Asus', 'X540YA', '1', '2019-08-29 12:59:59', '2019-08-29 14:59:52'),
        (6, 'Laptop', 'Asus', 'E202SA', '2', '2019-08-29 12:59:59', '2019-08-29 14:59:52')
        ");
    }
}
