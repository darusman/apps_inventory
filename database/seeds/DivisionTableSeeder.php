<?php

use Illuminate\Database\Seeder;

class DivisionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::insert("INSERT INTO `divisions` (`id`, `name`,  `created_at`, `updated_at`) VALUES
        (1, 'Sekretaris','2019-08-29 12:55:52', '2019-08-29 12:55:52'),
        (2, 'Bidang Pemukiman & Prasarana Wilayah','2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (3, 'Bidang Ekonomi','2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (4, 'Bidang Pemerintahan Umum & Aparatur','2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (5, 'Bidang Sosial Budaya','2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (6, 'Bidang Pendataan dan Statistik','2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (7, 'Kelompok Jabatan Fungsional','2019-08-29 12:55:59', '2019-08-29 13:55:52')
        ");
    }
}
