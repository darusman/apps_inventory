<?php

use Illuminate\Database\Seeder;

class NetworkDeviceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::insert("INSERT INTO `network_devices` (`id`, `hostname`, `IP_Address`,  `created_at`, `updated_at`) VALUES
        (1, 'bappeda.go.id', '192.168.10.10', '2019-08-29 12:55:52', '2019-08-29 12:55:52'),
        (2, 'bappeda.go.id', '192.168.10.20','2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (3, 'bappeda.go.id', '192.168.10.30', '2019-08-29 12:55:52', '2019-08-29 12:55:52'),
        (4, 'bappeda.go.id', '192.168.10.40', '2019-08-29 12:55:52', '2019-08-29 12:55:52'),
        (5, 'bappeda.go.id', '192.168.10.50', '2019-08-29 12:55:52', '2019-08-29 12:55:52'),
        (6, 'bappeda.go.id', '192.168.10.60', '2019-08-29 12:55:52', '2019-08-29 12:55:52'),
        (7, 'bappeda.go.id', '192.168.10.70', '2019-08-29 12:55:52', '2019-08-29 12:55:52'),
        (8, 'bappeda.go.id', '192.168.10.80', '2019-08-29 12:55:52', '2019-08-29 12:55:52'),
        (9, 'bappeda.go.id', '192.168.10.90', '2019-08-29 12:55:52', '2019-08-29 12:55:52')");
    }
}
