<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //ini untuk manggil seeder
        // $this->call(UsersTableSeeder::class);
        //$this->call(OriginTableSeeder::class);
        $this->call(DivisionTableSeeder::class);
        $this->call(NetworkDeviceTableSeeder::class);
        $this->call(DeviceTypeTableSeeder::class);
        $this->call(SubDivisionTableSeeder::class);
        $this->call(UsersTableSeeder::class); 
        
        
    }
}
