<?php

use Illuminate\Database\Seeder;

class SubDivisionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::insert("INSERT INTO `sub_divisions` (`id`, `name`, `divisions_id`,  `created_at`, `updated_at`) VALUES
        (1, 'Sub Bag Umum dan Kepegawaian', '1','2019-08-29 12:55:52', '2019-08-29 12:55:52'),
        (2, 'Sub Bag Keuangan', '1','2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (3, 'Sub Bag Program dan Laporan', '1', '2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (4, 'Sub Bidang Sumber Daya Air dan Keciptakaryaan', '2','2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (5, 'Sub Bidang Perhubungan dan Penataan Ruang', '2', '2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (6, 'Sub Bidang Pertanian', '3', '2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (7, 'Sub Bidang Perekonomian', '3', '2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (8, 'Sub Bidang Pemerintahan Umum', '4', '2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (9, 'Sub Bidang Kependudukan dan Aparatur', '4', '2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (10, 'Sub Bidang Pendidikan & Kebudayaan', '5', '2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (11, 'Sub Bidang Sosial & Tenaga Kerja', '5', '2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (12, 'Sub Bidang Pendataan', '6', '2019-08-29 12:55:59', '2019-08-29 13:55:52'),
        (13, 'Sub Bidang Statistik', '6', '2019-08-29 12:55:59', '2019-08-29 13:55:52')
        ");
    }
}
